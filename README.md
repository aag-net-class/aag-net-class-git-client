# Git Client Docker Container

Use the container directly or create an alias like
`alias git='docker run -it --rm -u 1100:1100 -v ~/.ssh:/home/user/.ssh -v $(pwd):/home/user/git'`
(see also the examples directory) and then use it as usual, e.g. git clone <Git-Repository>

## How to build and publish

1. Execute Docker builder, e.g.
   `docker build -t registry.gitlab.com/aag-net-class/aag-net-class-git-client:rangeros-20180121-0033 -t registry.gitlab.com/aag-net-class/aag-net-class-git-client:rangeros-latest rangeros/`
2. After testing, upload the image to the registry with `docker push registry.gitlab.com/aag-net-class/aag-net-class-git-client`
